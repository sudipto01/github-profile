const bc = new BroadcastChannel('broad')

$("form").submit(async e => {
   e.target.user.blur()
   e.preventDefault()
   
   let user = e.target.user.value
   if (!user) {
      alert("Enter username to continue!")
      return
   }
   user = user.trim()
   
   const url = `https://api.github.com/users/${user}`
   const res = await fetch(url)
   
   const data = await res.json()
   if (!data.message) {
      bc.postMessage(data)
      
      $("img").eq(0).attr("src", data.avatar_url)
      $("#gitstats").text("📊 GitHub Stats:")
      $("img").eq(1).attr("src", `https://github-readme-stats.vercel.app/api?username=${data.login}&theme=gruvbox&include_all_commits=false&count_private=false`)
      $("img").eq(2).attr("src", `https://github-readme-streak-stats.herokuapp.com/?user=${data.login}&theme=gruvbox`)
      $("img").eq(3).attr("src", `https://github-readme-stats.vercel.app/api/top-langs/?username=${data.login}&theme=gruvbox&include_all_commits=false&count_priavte=false&layout=compact`)
      $("img").eq(4).attr("src", `https://github-contributor-stats.vercel.app/api?username=${data.login}&limit=5&theme=vue-dark&combine_all_yearly_contributions=true`)
      
      $("#followers").html(`${data.followers}<br>followers`)
      $("#following").html(`${data.following}<br>following`)
      
      let txt = `Id: ${data.id}<br>`
      if (data.public_repos)
         txt += `Public repos: ${data.public_repos}<br>`
      if (data.name)
         if (data.hireable)
            txt += `Name: ${data.name} (Open to work)<br>`
         else
            txt += `Name: ${data.name}<br>`
      txt += `Username: ${data.login}<br>`
      if (data.company)
         txt += `Company: ${data.company}<br>`
      if (data.location)
         txt += `Location: ${data.location}<br>`
      if (data.email)
         txt += `Email: ${data.email}<br>`
      if (data.bio)
         txt += `Bio: ${data.bio}<br>`
      txt += `Created: ${getTime(data.created_at)}<br>
      Last updated: ${getTime(data.updated_at)}`
      
      const profile = $(`<p class='links'><a href='${data.html_url}' target='_main'>Profile</a></p>`)
      $("#links").text("").append(profile)
      
      if (data.blog) {
         const blog = $(`<p class='links'><a href='${data.blog}' target='_main'>Blog</a></p>`)
         $("#links").append(blog)
      }
      if (data.twitter_username) {
         const twitter = $(`<p class='links'><a href='https://twitter.com/${data.twitter_username}' target='_main'>Twitter</a></p>`)
         $("#links").append(twitter)
      }
      
      
      $("#fill").html(txt).removeClass("error")
   } else {
      alert(data.message)
      $("#fill").text("")
      .append(data.message, "<br><br>", `<a href="${data.documentation_url}">Documentation</a>`)
      .addClass("error")
   }
})

function getTime(tm) {
   return new Date(tm).toLocaleString()
}
function getHeaders(res) {
   const headers = res.headers;
    
   const headersObject = {};
   for (const [key, value] of headers.entries()) {
      headersObject[key] = value;
      $("#msg").append(key, ":", value, "<br>")
   }
    
   const contentType = headers.get('content-type');
   bc.postMessage(headers)
}
